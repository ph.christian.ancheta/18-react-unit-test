# React Unit Test - React App
A simple React App that showcases the process flow of unit tests through the jest suite. More of a theoretical project, this application is created only for making tests in React.

## Setup

```
npm install
npm run test
```
